const path = require('path');

module.exports = exports = env => ({
    entry: './index.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, './public/')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            }
        ]
    }
})
