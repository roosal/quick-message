import QuickMessage from '../src/quick-message';
import quickMessage from '../src/quick-message';

// Initialize data
let data = {
    theme: true,
    type: 'info',
    text: '',
    quickMessage: {
        theme: 'bright',
        transition: 'fast',
        stagger: 'short',
        class: 'quick-message'
    },
    randomTexts: [
        'Let\'s give this some good testing',
        'If you really wanted a way too long informational message. Wauw you are in the right place',
        'I like big borders and I can not lie',
        'Red is the new black and white',
        'All hail the random text generator',
        'Did I already tell you that this is a message',
        'User could not be added to the database of awesome at the moment',
        'Please try again later'
    ]
}

// Create the Quick messenger
let quickMessenger = QuickMessage(data.quickMessage);

// Add event listeners
document.getElementById('theme').addEventListener('change', function (event) {
    data.quickMessage.theme = this.value;
});

document.getElementById('className').addEventListener('change', function (event) {
    data.quickMessage.class = this.value;
});

document.querySelectorAll('[name=transition]').forEach(function(transition) {
    transition.addEventListener('change', function (event) {
        document.querySelectorAll('[name=transition]').forEach(function (element) {
            if (element.checked) {
                data.quickMessage.transition = element.value;
            }
        });
    });
});

document.querySelectorAll('[name=stagger]').forEach(function(stagger) {
    stagger.addEventListener('change', function (event) {
        document.querySelectorAll('[name=stagger]').forEach(function (element) {
            if (element.checked) {
                data.quickMessage.stagger = element.value;
            }
        });
    });
});

document.querySelectorAll('[name=messageType]').forEach(function(type) {
    type.addEventListener('change', function (event) {
        document.querySelectorAll('[name=messageType]').forEach(function (element) {
            if (element.checked) {
                data.type = element.value;
            }
        });
    });
});

document.getElementById('sendMyText').addEventListener('click', function(event) {
    data.text = document.getElementById('myText').value;
    if(data.text === '') {
        return console.log('There is no message to send!');
    }
    quickMessenger = QuickMessage(data.quickMessage);
    quickMessenger.message(data.type)(data.text);
});

document.getElementById('sendRandom').addEventListener('click', function(event) {
    data.text = data.randomTexts[Math.floor(Math.random() * data.randomTexts.length)];
    quickMessenger = QuickMessage(data.quickMessage);
    quickMessenger.message(data.type)(data.text);
});

document.getElementById('themeSwitch').addEventListener('click', function(event) {
    const themed = document.getElementById('themed');
    const custom = document.getElementById('custom');
    
    if(!data.theme) {
        // Enable themed
        // themed.style.opacity = '1';
        let themeChilds = themed.children;
        let customChilds = custom.children;
        for(let i = 0; i < themeChilds.length; i++) {
            themeChilds[i].classList.remove('faded');
        }
        for(let i = 0; i < customChilds.length; i++) {
            customChilds[i].classList.add('faded');
        }
        themed.querySelector('#theme').disabled = false;
        // Disable custom
        // custom.style.opacity = '0.4';
        custom.querySelector('#className').disabled = true;
        // Set data
        data.theme = true;
        data.quickMessage.class = 'quick-message';
    }
});

document.getElementById('customSwitch').addEventListener('click', function(event) {
    const themed = document.getElementById('themed');
    const custom = document.getElementById('custom');
    
    if(data.theme) {
        // Disable themed
        let themeChilds = themed.children;
        let customChilds = custom.children;
        for(let i = 0; i < themeChilds.length; i++) {
            themeChilds[i].classList.add('faded');
        }
        for(let i = 0; i < customChilds.length; i++) {
            customChilds[i].classList.remove('faded');
        }
        themed.querySelector('#theme').disabled = true;
        // Disable custom
        // custom.style.opacity = '1';
        custom.querySelector('#className').disabled = false;
        // Set data
        data.theme = false;
        data.quickMessage.class = custom.querySelector('#className').value;
    }
});
