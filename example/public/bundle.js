/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 *              Quick Message
 * ---------------------------------------------
 * 
 *             configuration.js
 * 
 * ---------------------------------------------
 * Helper file for validating user provided
 * configuration options.
 * ---------------------------------------------
 */

/**
 * An array containing all available themes as strings
 */
const availableThemes = ['bright', 'faded'];
/* unused harmony export availableThemes */


/**
 * An object mapping the stagger speeds as strings to number values
 */
const stagger = {
  short: 10,
  long: 200
};
/* unused harmony export stagger */


/**
 * An object mapping the transition speeds as strings to number values
 */
const transition = {
  none: .01,
  fast: .2,
  normal: .5,
  slow: 1

  /**
   * Create default export object with all exported functions
   */
};
/* unused harmony export transition */
/* harmony default export */ __webpack_exports__["a"] = ({
  availableThemes,
  stagger,
  transition
});

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _quickMessage = __webpack_require__(2);

var _quickMessage2 = _interopRequireDefault(_quickMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Initialize data
var data = {
    theme: true,
    type: 'info',
    text: '',
    quickMessage: {
        theme: 'bright',
        transition: 'fast',
        stagger: 'short',
        class: 'quick-message'
    },
    randomTexts: ['Let\'s give this some good testing', 'If you really wanted a way too long informational message. Wauw you are in the right place', 'I like big borders and I can not lie', 'Red is the new black and white', 'All hail the random text generator', 'Did I already tell you that this is a message', 'User could not be added to the database of awesome at the moment', 'Please try again later']

    // Create the Quick messenger
};var quickMessenger = (0, _quickMessage2.default)(data.quickMessage);

// Add event listeners
document.getElementById('theme').addEventListener('change', function (event) {
    data.quickMessage.theme = this.value;
});

document.getElementById('className').addEventListener('change', function (event) {
    data.quickMessage.class = this.value;
});

document.querySelectorAll('[name=transition]').forEach(function (transition) {
    transition.addEventListener('change', function (event) {
        document.querySelectorAll('[name=transition]').forEach(function (element) {
            if (element.checked) {
                data.quickMessage.transition = element.value;
            }
        });
    });
});

document.querySelectorAll('[name=stagger]').forEach(function (stagger) {
    stagger.addEventListener('change', function (event) {
        document.querySelectorAll('[name=stagger]').forEach(function (element) {
            if (element.checked) {
                data.quickMessage.stagger = element.value;
            }
        });
    });
});

document.querySelectorAll('[name=messageType]').forEach(function (type) {
    type.addEventListener('change', function (event) {
        document.querySelectorAll('[name=messageType]').forEach(function (element) {
            if (element.checked) {
                data.type = element.value;
            }
        });
    });
});

document.getElementById('sendMyText').addEventListener('click', function (event) {
    data.text = document.getElementById('myText').value;
    if (data.text === '') {
        return console.log('There is no message to send!');
    }
    quickMessenger = (0, _quickMessage2.default)(data.quickMessage);
    quickMessenger.message(data.type)(data.text);
});

document.getElementById('sendRandom').addEventListener('click', function (event) {
    data.text = data.randomTexts[Math.floor(Math.random() * data.randomTexts.length)];
    quickMessenger = (0, _quickMessage2.default)(data.quickMessage);
    quickMessenger.message(data.type)(data.text);
});

document.getElementById('themeSwitch').addEventListener('click', function (event) {
    var themed = document.getElementById('themed');
    var custom = document.getElementById('custom');

    if (!data.theme) {
        // Enable themed
        // themed.style.opacity = '1';
        var themeChilds = themed.children;
        var customChilds = custom.children;
        for (var i = 0; i < themeChilds.length; i++) {
            themeChilds[i].classList.remove('faded');
        }
        for (var _i = 0; _i < customChilds.length; _i++) {
            customChilds[_i].classList.add('faded');
        }
        themed.querySelector('#theme').disabled = false;
        // Disable custom
        // custom.style.opacity = '0.4';
        custom.querySelector('#className').disabled = true;
        // Set data
        data.theme = true;
        data.quickMessage.class = 'quick-message';
    }
});

document.getElementById('customSwitch').addEventListener('click', function (event) {
    var themed = document.getElementById('themed');
    var custom = document.getElementById('custom');

    if (data.theme) {
        // Disable themed
        var themeChilds = themed.children;
        var customChilds = custom.children;
        for (var i = 0; i < themeChilds.length; i++) {
            themeChilds[i].classList.add('faded');
        }
        for (var _i2 = 0; _i2 < customChilds.length; _i2++) {
            customChilds[_i2].classList.remove('faded');
        }
        themed.querySelector('#theme').disabled = true;
        // Disable custom
        // custom.style.opacity = '1';
        custom.querySelector('#className').disabled = false;
        // Set data
        data.theme = false;
        data.quickMessage.class = custom.querySelector('#className').value;
    }
});

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__lib_validation__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__lib_configuration__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__themes_index__ = __webpack_require__(4);
/**
 * Quick Message - Print messages, the good way
 */





/**
 * createQuickMessage - Setup some shared variables to ensure correct functioning of the created Quick Messengers
 */
/* harmony default export */ __webpack_exports__["default"] = ((function createQuickMessage() {

	// Check if we are in a browser
	if (typeof window === undefined) {
		return console.log('Quick Message can only be used in a browser');
	}

	// Setup internal variables
	let queue = [];
	let timer;
	let index = 0;
	let anchor = document.createElement('div');

	anchor.style.position = 'fixed';
	anchor.style.left = '-1px';
	anchor.style.top = '0';
	document.body.appendChild(anchor);

	/**
  * The default export of Quick Message is a function which takes an options object and returns an object with a number of print methods
  * 
  * Options {
  *   transitions: 	'none', 'fast', 'normal', 'slow'	- How fast the animations run for the messages. Defaults to fast
  *   stagger: 		'long', 'short'						- The minimum time between messages. Defautls to short
  *   theme: 		'bright', 'faded'					- Which built-in theme to use for the messages. Defaults to bright
  *   class: 		'<string>'							- This class will be used on the messages. Using this option prevents built-in themes. Default to quick-message
  * }
  * 
  * Public API {
  *   message: 	function(type: <string>): function(message: <string>): Object { type: <string>, message: <string> }
  *   info: 		function(message: <string>): Object { type: 'info', message: <string> }
  *   warning:	function(message: <string>): Object { type: 'warning', message: <string> }
  *   success:	function(message: <string>): Object { type: 'success', message: <string> }
  *   error:		function(message: <string>): Object { type: 'error', message: <string> }
  * }
  */
	return options => {

		// Complete missing options
		options = typeof options === 'object' && typeof options !== 'null' ? options : {};
		options.transition = options.transition || 'fast';
		options.stagger = options.stagger || 'short';
		options.theme = options.theme || 'bright';
		options.class = options.class || 'quick-message';

		console.log(options);

		// Check options passed along, will throw when an invalid option is passed
		__WEBPACK_IMPORTED_MODULE_0__lib_validation__["a" /* default */].validateOptions(options);

		// TODO - check if this way of working is feasible
		// Add css theme style to the document
		let quickMessageStyle = document.createElement('style');
		quickMessageStyle.innerHTML = __WEBPACK_IMPORTED_MODULE_2__themes_index__["a" /* default */][options.theme];
		quickMessageStyle.rel = 'stylesheet';
		quickMessageStyle.id = `QuickMessage${options.theme}theme`;

		if (document.getElementById(`QuickMessageTheme`) === null) {
			document.head.appendChild(quickMessageStyle);
		} else {
			document.head.replaceChild(quickMessageStyle, document.getElementById(`QuickMessageTheme`));
		}

		/**
   * The Quick Message Core Function
   * Pass a type of message to create a specialized message function, then pass the actual message
   * Eg. QuickMessage('info')('message')
   */
		const quickMessage = type => message => {

			// Push a new message in the printer
			queue.push({ type, message });

			// Start printing if there are is no activity
			if (timer === undefined) {
				timer = setInterval(print, 100);
			}

			/**
    * print - Prints the next message in the printer queue
    */
			function print() {

				let nextMessage = queue.shift();
				let messageElement = build(nextMessage);

				index += 1;

				// Add message to DOM
				anchor.appendChild(messageElement);
				// Add timeout of 10ms to get better scaffolding of messages in quick succession
				setTimeout(() => messageElement.classList.remove('anim-out'), __WEBPACK_IMPORTED_MODULE_1__lib_configuration__["a" /* default */].stagger[options.stagger]);

				// Remove message from the DOM after 5s
				setTimeout(() => messageElement.classList.add('anim-out'), 5000);

				// Stop printing messages if there are none in the bus
				if (queue.length === 0) {
					clearInterval(timer);
					timer = undefined;
				}
			}

			/**
    * build - Build the DOM structure for a message
    * @param {Object} next - An object containing a message and a type property which both are strings
    * @return {HTMLElement}
    */
			function build(next) {
				// Create html structure
				let messageEl = document.createElement('div');

				messageEl.className = `${options.class} anim-out ${next.type}`;
				messageEl.innerHTML = next.message;
				// messageEl.style.top = `${index * 40}px`;
				messageEl.style.float = 'left';
				messageEl.style.clear = 'both';
				messageEl.style.position = 'relative';
				messageEl.style.transition = `all ${__WEBPACK_IMPORTED_MODULE_1__lib_configuration__["a" /* default */].transition[options.transition]}s`;

				// Handle end of transition for removing itself from the DOM
				messageEl.addEventListener('transitionend', event => {
					if (messageEl.classList.contains('anim-out') && messageEl.parentNode !== null) {
						anchor.removeChild(messageEl);
						index -= 1;
					}
				});

				return messageEl;
			}
		};

		/**
   * Public API of QuickMessage Module
   */
		return {
			message: quickMessage,
			info: quickMessage('info'),
			success: quickMessage('success'),
			error: quickMessage('error'),
			warning: quickMessage('warning')
		};
	};
})());

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export isValidTheme */
/* unused harmony export isValidStagger */
/* unused harmony export isValidTransition */
/* unused harmony export validateOptions */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__configuration__ = __webpack_require__(0);
/**
 *              Quick Message
 * ---------------------------------------------
 * 
 *              validation.js
 * 
 * ---------------------------------------------
 * Helper file for validating user provided
 * configuration options.
 * ---------------------------------------------
 */



/**
 * isValidTheme - Check if the provided theme is valid
 * @param {string} theme - The name of the theme to validate
 * @return {boolean} 
 */
function isValidTheme(theme) {
  if (typeof theme !== 'string') throw TypeError(`The theme option needs to be of value string`);
  return __WEBPACK_IMPORTED_MODULE_0__configuration__["a" /* default */].availableThemes.indexOf(theme) !== -1;
}

/**
 * isValidStagger - Check if the provided stagger value is valid
 * @param {string} stagger - The stagger value to validate
 * @return {boolean} 
 */
function isValidStagger(stagger) {
  if (typeof stagger !== 'string') throw TypeError(`The stagger option needs to be of value string`);
  return Object.keys(__WEBPACK_IMPORTED_MODULE_0__configuration__["a" /* default */].stagger).indexOf(stagger) !== -1;
}

/**
 * isValidTransition - Check if the provided theme is valid
 * @param {string} transition - The transition value to validate
 * @return {boolean} 
 */
function isValidTransition(transition) {
  if (typeof transition !== 'string') throw TypeError(`The transition option needs to be of value string`);
  return Object.keys(__WEBPACK_IMPORTED_MODULE_0__configuration__["a" /* default */].transition).indexOf(transition) !== -1;
}

/**
 * Validate all the options for Quick Message
 * @param {Object} options
 * @throws Error 
 */
function validateOptions(options) {
  if (!isValidTheme(options.theme)) throw Error(`Invalid theme chosen, available options are ${[...__WEBPACK_IMPORTED_MODULE_0__configuration__["a" /* default */].availableThemes]}`);
  if (!isValidStagger(options.stagger)) throw Error(`Invalid stagger chosen, available options are ${[...Object.keys(__WEBPACK_IMPORTED_MODULE_0__configuration__["a" /* default */].stagger)]}`);
  if (!isValidTransition(options.transition)) throw Error(`Invalid transition chosen, available options are ${[...Object.keys(__WEBPACK_IMPORTED_MODULE_0__configuration__["a" /* default */].transition)]}`);
}

/**
 * Create default export object with all exported functions
 */
/* harmony default export */ __webpack_exports__["a"] = ({
  isValidTheme,
  isValidStagger,
  isValidTransition,
  validateOptions
});

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__bright__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__faded__ = __webpack_require__(6);



/* harmony default export */ __webpack_exports__["a"] = ({ bright: __WEBPACK_IMPORTED_MODULE_0__bright__["a" /* default */], faded: __WEBPACK_IMPORTED_MODULE_1__faded__["a" /* default */] });

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 *      Define Theme style for Quick Message
 * --------------------------------------------------
 * 
 *              bright theme style
 * 
 * --------------------------------------------------
 * This string will be injected in a style tag in the
 * document where the project resides.  
 */

/* harmony default export */ __webpack_exports__["a"] = (`

.quick-message {
	background-color:rgb(150, 250, 150);
	border-radius: 0 5px 5px 0;
    box-shadow: 1px 1px rgba(0, 0, 0, 0.5);
    color: white;
    left: -1px;
    top: 0;
	opacity: 1;
    padding: 8px 40px;
    text-shadow: 1px 1px #000;
    margin-top: 5px;
}

.anim-out {
	opacity: 0;
	left: -100px;
}

.quick-message.success {
    border: 1px solid olivedrab;
    background: #76c153;
    background: linear-gradient(to bottom, #76c153, #6aaa4c);
}

.quick-message.error {
    border: 1px solid darkred;
    background: indianred;
    background: linear-gradient(to bottom, indianred, sienna);
}

.quick-message.warning {
    border: 1px solid darkkhaki;
    background: moccasin;
    background: linear-gradient(to bottom, moccasin, khaki);
}

.quick-message.info {
    border: 1px solid steelblue;
    background: skyblue;
    background: linear-gradient(to bottom, skyblue, cadetblue);
}

`);

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 *      Define Theme style for Quick Message
 * --------------------------------------------------
 * 
 *              faded theme style
 * 
 * --------------------------------------------------
 * This string will be injected in a style tag in the
 * document where the project resides.  
 */

/* harmony default export */ __webpack_exports__["a"] = (`

.quick-message {
	background-color:rgb(150, 250, 150);
	border-radius: 0 5px 5px 0;
    color: #000;
	left: -1px;
	margin: 5px 0 0 0;
	opacity: 1;
	padding: 10px 40px;
}

.anim-out {
	opacity: 0;
	left: -100px;
}

.quick-message.success {
    border: 1px solid palegoldenrod;
    background: beige;
}

.quick-message.error {
    border: 1px solid mistyrose;
    background: linen;
}

.quick-message.warning {
    border: 1px solid darkkhaki;
    background: moccasin;
}

.quick-message.info {
    border: 1px solid wheat;
    background: lemonchiffon;
}

`);

/***/ })
/******/ ]);