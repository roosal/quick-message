/**
 *      Define Theme style for Quick Message
 * --------------------------------------------------
 * 
 *              faded theme style
 * 
 * --------------------------------------------------
 * This string will be injected in a style tag in the
 * document where the project resides.  
 */

export default `

.quick-message {
	background-color:rgb(150, 250, 150);
	border-radius: 0 5px 5px 0;
    color: #000;
	left: -1px;
	margin: 5px 0 0 0;
	opacity: 1;
	padding: 10px 40px;
}

.anim-out {
	opacity: 0;
	left: -100px;
}

.quick-message.success {
    border: 1px solid palegoldenrod;
    background: beige;
}

.quick-message.error {
    border: 1px solid mistyrose;
    background: linen;
}

.quick-message.warning {
    border: 1px solid darkkhaki;
    background: moccasin;
}

.quick-message.info {
    border: 1px solid wheat;
    background: lemonchiffon;
}

`;