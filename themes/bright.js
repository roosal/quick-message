/**
 *      Define Theme style for Quick Message
 * --------------------------------------------------
 * 
 *              bright theme style
 * 
 * --------------------------------------------------
 * This string will be injected in a style tag in the
 * document where the project resides.  
 */

export default `

.quick-message {
	background-color:rgb(150, 250, 150);
	border-radius: 0 5px 5px 0;
    box-shadow: 1px 1px rgba(0, 0, 0, 0.5);
    color: white;
    left: -1px;
    top: 0;
	opacity: 1;
    padding: 8px 40px;
    text-shadow: 1px 1px #000;
    margin-top: 5px;
}

.anim-out {
	opacity: 0;
	left: -100px;
}

.quick-message.success {
    border: 1px solid olivedrab;
    background: #76c153;
    background: linear-gradient(to bottom, #76c153, #6aaa4c);
}

.quick-message.error {
    border: 1px solid darkred;
    background: indianred;
    background: linear-gradient(to bottom, indianred, sienna);
}

.quick-message.warning {
    border: 1px solid darkkhaki;
    background: moccasin;
    background: linear-gradient(to bottom, moccasin, khaki);
}

.quick-message.info {
    border: 1px solid steelblue;
    background: skyblue;
    background: linear-gradient(to bottom, skyblue, cadetblue);
}

`;