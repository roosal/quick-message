import bright from './bright';
import faded from './faded';

export default { bright, faded };