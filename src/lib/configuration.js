/**
 *              Quick Message
 * ---------------------------------------------
 * 
 *             configuration.js
 * 
 * ---------------------------------------------
 * Helper file for validating user provided
 * configuration options.
 * ---------------------------------------------
 */

/**
 * An array containing all available themes as strings
 */
export const availableThemes = [
    'bright',
    'faded'
];

/**
 * An object mapping the stagger speeds as strings to number values
 */
export const stagger = {
    short: 10,
    long: 200
};

/**
 * An object mapping the transition speeds as strings to number values
 */
export const transition = {
    none: .01,
    fast: .2,
    normal: .5,
    slow: 1
}

/**
 * Create default export object with all exported functions
 */
export default {
    availableThemes,
    stagger,
    transition
}
