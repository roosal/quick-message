/**
 *              Quick Message
 * ---------------------------------------------
 * 
 *              validation.js
 * 
 * ---------------------------------------------
 * Helper file for validating user provided
 * configuration options.
 * ---------------------------------------------
 */

import config from './configuration';


/**
 * isValidTheme - Check if the provided theme is valid
 * @param {string} theme - The name of the theme to validate
 * @return {boolean} 
 */
export function isValidTheme(theme) {
    if(typeof theme !== 'string') throw TypeError(`The theme option needs to be of value string`);
    return config.availableThemes.indexOf(theme) !== -1;
}

/**
 * isValidStagger - Check if the provided stagger value is valid
 * @param {string} stagger - The stagger value to validate
 * @return {boolean} 
 */
export function isValidStagger(stagger) {
    if(typeof stagger !== 'string') throw TypeError(`The stagger option needs to be of value string`);
    return Object.keys(config.stagger).indexOf(stagger) !== -1;
}

/**
 * isValidTransition - Check if the provided theme is valid
 * @param {string} transition - The transition value to validate
 * @return {boolean} 
 */
export function isValidTransition(transition) {
    if(typeof transition !== 'string') throw TypeError(`The transition option needs to be of value string`);
    return Object.keys(config.transition).indexOf(transition) !== -1;
}

/**
 * Validate all the options for Quick Message
 * @param {Object} options
 * @throws Error 
 */
export function validateOptions(options) {
    if(!isValidTheme(options.theme)) throw Error(`Invalid theme chosen, available options are ${[...config.availableThemes]}`);
    if(!isValidStagger(options.stagger)) throw Error(`Invalid stagger chosen, available options are ${[...Object.keys(config.stagger)]}`);
    if(!isValidTransition(options.transition)) throw Error(`Invalid transition chosen, available options are ${[...Object.keys(config.transition)]}`);
}

/**
 * Create default export object with all exported functions
 */
export default {
    isValidTheme,
    isValidStagger,
    isValidTransition,
    validateOptions
}
