/**
 * Quick Message - Print messages, the good way
 */

import validation from './lib/validation';
import configuration from './lib/configuration';
import themes from '../themes/index';

/**
 * createQuickMessage - Setup some shared variables to ensure correct functioning of the created Quick Messengers
 */
export default (function createQuickMessage() {

	// Check if we are in a browser
	if(typeof window === undefined) {
		return console.log('Quick Message can only be used in a browser');
	}

	// Setup internal variables
	let queue = [];
	let timer;
	let index = 0;
	let anchor = document.createElement('div');

	anchor.style.position = 'fixed';
	anchor.style.left = '-1px';
	anchor.style.top = '0';
	document.body.appendChild(anchor);

	/**
	 * The default export of Quick Message is a function which takes an options object and returns an object with a number of print methods
	 * 
	 * Options {
	 *   transitions: 	'none', 'fast', 'normal', 'slow'	- How fast the animations run for the messages. Defaults to fast
	 *   stagger: 		'long', 'short'						- The minimum time between messages. Defautls to short
	 *   theme: 		'bright', 'faded'					- Which built-in theme to use for the messages. Defaults to bright
	 *   class: 		'<string>'							- This class will be used on the messages. Using this option prevents built-in themes. Default to quick-message
	 * }
	 * 
	 * Public API {
	 *   message: 	function(type: <string>): function(message: <string>): Object { type: <string>, message: <string> }
	 *   info: 		function(message: <string>): Object { type: 'info', message: <string> }
	 *   warning:	function(message: <string>): Object { type: 'warning', message: <string> }
	 *   success:	function(message: <string>): Object { type: 'success', message: <string> }
	 *   error:		function(message: <string>): Object { type: 'error', message: <string> }
	 * }
	 */
	return options => {

		// Complete missing options
		options = (typeof options === 'object' && typeof options !== 'null') ? options : {};
		options.transition = options.transition || 'fast';
		options.stagger = options.stagger || 'short';
		options.theme = options.theme || 'bright';
		options.class = options.class || 'quick-message';

		console.log(options);

		// Check options passed along, will throw when an invalid option is passed
		validation.validateOptions(options);

		// TODO - check if this way of working is feasible
		// Add css theme style to the document
		let quickMessageStyle = document.createElement('style');
		quickMessageStyle.innerHTML = themes[options.theme];
		quickMessageStyle.rel = 'stylesheet';
		quickMessageStyle.id = `QuickMessage${options.theme}theme`;
		
		if(document.getElementById(`QuickMessageTheme`) === null) {
			document.head.appendChild(quickMessageStyle);
		} else {
			document.head.replaceChild(quickMessageStyle, document.getElementById(`QuickMessageTheme`));
		}

		/**
		 * The Quick Message Core Function
		 * Pass a type of message to create a specialized message function, then pass the actual message
		 * Eg. QuickMessage('info')('message')
		 */
		const quickMessage = type => message => {

			// Push a new message in the printer
			queue.push({ type, message });

			// Start printing if there are is no activity
			if(timer === undefined) {
				timer = setInterval(print, 100);
			}

			/**
			 * print - Prints the next message in the printer queue
			 */
			function print() {

				let nextMessage = queue.shift();
				let messageElement = build(nextMessage);

				index += 1;

				// Add message to DOM
				anchor.appendChild(messageElement);
				// Add timeout of 10ms to get better scaffolding of messages in quick succession
				setTimeout(() => messageElement.classList.remove('anim-out'), configuration.stagger[options.stagger]);

				// Remove message from the DOM after 5s
				setTimeout(() => messageElement.classList.add('anim-out'), 5000);

				// Stop printing messages if there are none in the bus
				if(queue.length === 0) {
					clearInterval(timer);
					timer = undefined;
				}
			}

			/**
			 * build - Build the DOM structure for a message
			 * @param {Object} next - An object containing a message and a type property which both are strings
			 * @return {HTMLElement}
			 */
			function build(next) {
				// Create html structure
				let messageEl = document.createElement('div');

				messageEl.className = `${options.class} anim-out ${next.type}`;
				messageEl.innerHTML = next.message;
				// messageEl.style.top = `${index * 40}px`;
				messageEl.style.float = 'left';
				messageEl.style.clear = 'both';
				messageEl.style.position = 'relative';
				messageEl.style.transition = `all ${configuration.transition[options.transition]}s`;

				// Handle end of transition for removing itself from the DOM
				messageEl.addEventListener('transitionend', event => {
					if(messageEl.classList.contains('anim-out') && messageEl.parentNode !== null) {
						anchor.removeChild(messageEl);
						index -= 1;
					}
				});

				return messageEl;
			}
		}

		/**
		 * Public API of QuickMessage Module
		 */
		return {
			message: quickMessage,
			info: quickMessage('info'),
			success: quickMessage('success'),
			error: quickMessage('error'),
			warning: quickMessage('warning'),
		}

	}

}());
