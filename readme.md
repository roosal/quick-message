# Quick Message
A little tool for providing messages to your users. It is lightweight, customizable and easy to use.
The way Quick message works is non-obtrusive for the users. It are simple and fast messages, perfect for status updates and user actions.

## Message generator tool
Included in this project is a folder with a message generator. You can use this generator to test out all the different options and possibilities of the Quick Message tool.
To use this generator read the following steps

1. Download dependencies
In your terminal, go into the example folder and run npm install to get the required dependencies

2. Run webpack
Run npm run watch to generate the javascript bundle for the project.

3. Run the server
Run npm start to get the server running. You can find the server at http://localhost:1234
